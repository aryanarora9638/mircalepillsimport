//
//  ViewController.swift
//  MiraclePills
//
//  Created by Aryan Arora on 2018-05-12.
//  Copyright © 2018 Aryan Arora. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    let province = ["Alberta", "British Columbia", "Manitoba", "New Brunswick", "Newfoundland" , "Labrador", "Nova Scotia", "Ontario", "Prince Edward Island", "Quebec", "Saskatchewan"]
    
    
    @IBOutlet weak var provincePicker: UIPickerView!
    @IBOutlet weak var provincePickerbBtn: UIButton!
    override func viewDidLoad() {
        

        provincePicker.dataSource = self
        provincePicker.delegate = self
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func provinceBtnPressed(_ sender: Any) {
        if provincePicker.isHidden {//if true
            provincePicker.isHidden = false//hide it
        }
    }
    
    
    // returns the number of 'columns' to display.
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    // returns the # of rows in each component..
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return province.count
    }
    
    //returns the title to be displayed by a particular row
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return province[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        provincePickerbBtn.setTitle(province[row], for: .normal)
        provincePicker.isHidden = true
    }
}

